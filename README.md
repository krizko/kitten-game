# README #

### General Information ###

* https://www.reddit.com/r/kittensgame
* https://dojotoolkit.org/reference-guide/1.7/dojo/index.html#dojo-index (I'm using mostly pre-1.7 dojo functionality)

### Roadmap ###

* UI
    * Resources
        * Edit icon for craft table to show/hide individual craft recipes

* QOL/Other
    * Ziggurat upgrades should be hidden by default and unlocked one by one

* Features
    TBD

* Portability
    * Render/Update loops should be decoupled from the Game class.
        *  Game should be probably splitted into GameCore and GameClient with the option to provide arbitrary implementation of the GameClient
    * Most of the logic in the button handlers should be moved to the corresponding manager methods

Please feel free to add other suggestions.

### Contribution guidelines ###

TBD